#SPRING TRADING


SONAR TRADING Programming Challenge by _Cesar Laurentin_ (Oct-2018).

##Getting Started
These instructions will get you a copy of the project up and running on your local machine for evaluation purposes. 
Please follow the below instructions on how to run this project and read the notes on troubleshooting steps if needed.

###Prerequisites
- Up and Running spring-trading-config-server project (to support runtime configuration changes)
- A local clone of trading-config git repo (to make configuration changes on the fly through 
change-commit-push-refresh a zero downtime mechanism)
- Connectivity to Bitso and Bitbucket services
- Local port numbers 8888 and 8850 available

###Installing
1 Clone Repo:
```
git clone git@bitbucket.org:laurentin/spring-trading.git
```
2 Build and Install
```
> cd spring-trading
> mvn clean install
```
###Running
```
> spring-trading/target/java -jar -Dspring.profiles.active=dev spring-trading-0.0.1-SNAPSHOT.jar
```
##### Loading UI
```
http://localhost:8850/
```
###Changing runtime variables
This feature is backed up on spring config service and spring actuator, once you have changed any value on the spring-trading.yml file, which is under git repo trading-config, please commit and push your changes then you just need
to hit the actuator refresh endpoint:
```
    > curl --request POST http://localhost:8850/actuator/refresh
    > ["config.client.version","X.UI"]
```
####Verifying service
```
    > curl http://localhost:8850/actuator/health
    > {"status":"UP"}
```
###Checklist
Feature	> File name	> Method name

- Schedule the polling of trades over REST > TradesServicesImpl > startTradesPooling

- Request a book snapshot over REST > BookSnapshotServiceImpl > getBookSnapshot

- Listen for diff-orders over websocket > DiffOrderClient > onMessage

- Replay diff-orders > OrderServiceImpl  > asyncOrderProcessor

- Use config option X to request recent trades > TradesServicesImpl > getTrades

- Use config option X to limit number of ASKs displayed in UI > OrderServiceImpl > getBestOrders

- The loop that causes the trading algorithm to reevaluate > TradesServicesImpl > asyncContrarianProcessor


###Troubleshooting
- Check if you are running spring-trading-config-server project listening on port number 8888 before your try to run 
spring-trading project.
- You can verify if both projects are up and running hitting the actuator endpoint /health in each case
- Please check the console logs for specific details about warnings and error conditions


