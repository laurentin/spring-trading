package com.sonar.trading;

import com.sonar.trading.service.StartUpService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableAsync
public class SpringTradingApplication {

	@Autowired
	StartUpService startUpService;

	public static void main(String[] args) {
		SpringApplication.run(SpringTradingApplication.class, args);
	}

	@Bean
	public CommandLineRunner run() {
		return args ->
			startUpService.startSonarTradingFramework();
	}
}
