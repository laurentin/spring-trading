package com.sonar.trading.controller;

import com.sonar.trading.model.BestOrder;
import com.sonar.trading.service.OrderService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;




@RestController
@RequestMapping(value = "/orders")
@RefreshScope
public class OrdersController {

    private final Logger logger = LoggerFactory.getLogger("OrdersController");

    @Autowired
    private OrderService orderService;

    @Value("${X.UI}")
    private int size;

    @RequestMapping(value = "/best", method = RequestMethod.GET)
    public BestOrder getBestOrders() {
        logger.info("Getting the best {} Bids and Asks orders", size);
        return orderService.getBestOrders(size);
    }
}
