package com.sonar.trading.controller;

import com.sonar.trading.model.UITrade;
import com.sonar.trading.service.TradesServices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/trades")
@RefreshScope
public class TradesController {

    private final Logger logger = LoggerFactory.getLogger("TradesController");

    @Autowired
    private TradesServices tradesServices;

    @Value("${X.UI}")
    private int size;

    @RequestMapping(value = "/latest", method = RequestMethod.GET)
    public List<UITrade> getRecentTrades() {
        logger.info("Getting the best {} Buys and Sells trades", size);
        return tradesServices.getRecentTrades();
    }
}
