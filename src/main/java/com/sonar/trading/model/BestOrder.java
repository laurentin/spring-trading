package com.sonar.trading.model;


import java.util.List;

public class BestOrder {

    private String updated_at;
    private List<Order> asks;
    private List<Order> bids;


    public BestOrder() {
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public List<Order> getAsks() {
        return asks;
    }

    public void setAsks(List<Order> asks) {
        this.asks = asks;
    }

    public List<Order> getBids() {
        return bids;
    }

    public void setBids(List<Order> bids) {
        this.bids = bids;
    }
}
