package com.sonar.trading.model;

import java.math.BigDecimal;

public class ContrarianState {

    private BigDecimal price;
    private Integer upticks;
    private Integer downticks;

    public ContrarianState(BigDecimal price, Integer upticks, Integer downticks) {
        this.price = price;
        this.upticks = upticks;
        this.downticks = downticks;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getUpticks() {
        return upticks;
    }

    public void setUpticks(Integer upticks) {
        this.upticks = upticks;
    }

    public Integer getDownticks() {
        return downticks;
    }

    public void setDownticks(Integer downticks) {
        this.downticks = downticks;
    }
}
