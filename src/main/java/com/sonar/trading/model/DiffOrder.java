package com.sonar.trading.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

public class DiffOrder {

    private String type;
    private String book;
    private BigDecimal sequence;
    @JsonProperty("payload")
    private List<DiffOrderDetail> diffOrderDetail;

    public DiffOrder() {
    }

    public DiffOrder(String type, String book, BigDecimal sequence, List<DiffOrderDetail> diffOrderDetail) {
        this.type = type;
        this.book = book;
        this.sequence = sequence;
        this.diffOrderDetail = diffOrderDetail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public BigDecimal getSequence() {
        return sequence;
    }

    public void setSequence(BigDecimal sequence) {
        this.sequence = sequence;
    }

    public List<DiffOrderDetail> getDiffOrderDetail() {
        return diffOrderDetail;
    }

    public void setDiffOrderDetail(List<DiffOrderDetail> diffOrderDetail) {
        this.diffOrderDetail = diffOrderDetail;
    }

    @Override
    public String toString() {
        return "Diff-Order{type: " + type + "|"
                + "book: " + book + "|"
                + "sequence: " + sequence + "|"
                + "details: "+ diffOrderDetail + "}";
    }
}
