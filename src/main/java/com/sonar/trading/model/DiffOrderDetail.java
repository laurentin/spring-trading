package com.sonar.trading.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class DiffOrderDetail {

    @JsonProperty("o")
    private String orderId;
    @JsonProperty("d")
    private BigDecimal orderTimestamp;
    @JsonProperty("r")
    private String orderRate;
    @JsonProperty("t")
    private Integer orderType;
    @JsonProperty("a")
    private String orderAmount;
    @JsonProperty("v")
    private String orderValue;
    @JsonProperty("s")
    private String orderStatus;  // not defined in Bitso documentation

    public DiffOrderDetail() {
    }

    public DiffOrderDetail(String orderId, BigDecimal orderTimestamp, String orderRate, Integer orderType,
            String orderAmount, String orderValue, String orderStatus) {
        this.orderId = orderId;
        this.orderTimestamp = orderTimestamp;
        this.orderRate = orderRate;
        this.orderType = orderType;
        this.orderAmount = orderAmount;
        this.orderValue = orderValue;
        this.orderStatus = orderStatus;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getOrderTimestamp() {
        return orderTimestamp;
    }

    public void setOrderTimestamp(BigDecimal orderTimestamp) {
        this.orderTimestamp = orderTimestamp;
    }

    public String getOrderRate() {
        return orderRate;
    }

    public void setOrderRate(String orderRate) {
        this.orderRate = orderRate;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getOrderValue() {
        return orderValue;
    }

    public void setOrderValue(String orderValue) {
        this.orderValue = orderValue;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Override
    public String toString() {
        return "DiffOrderDetail{o: " + orderId + "|"
                + "d: " + orderTimestamp + "|"
                + "r: " + orderRate + "|"
                + "t: " + orderType + "|"
                + "a: " + orderAmount + "|"
                + "v: " + orderValue + "|"
                + "s: " + orderStatus + "}";
    }
}
