package com.sonar.trading.model;

import java.math.BigDecimal;
import java.util.List;

public class Ledger {

    private BigDecimal sequence;
    private String updateAt;
    private List<Order> bids;
    private List<Order> asks;

    public Ledger() {
        // populated by service layer
    }

    public BigDecimal getSequence() {
        return sequence;
    }

    public void setSequence(BigDecimal sequence) {
        this.sequence = sequence;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public List<Order> getBids() {
        return bids;
    }

    public void setBids(List<Order> bids) {
        this.bids = bids;
    }

    public List<Order> getAsks() {
        return asks;
    }

    public void setAsks(List<Order> asks) {
        this.asks = asks;
    }
}
