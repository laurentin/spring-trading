package com.sonar.trading.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

// Order represent bids or asks in an orderBook
public class Order implements Comparable {
    @JsonProperty("book")
    private String bookName;
    // Minor or Pesos MXN
    private String price;
    // Major or BTC
    private String amount;
    @JsonProperty("oid")
    private String orderId;
    @JsonIgnore
    private BigDecimal numericPrice;

    public Order() {
        // Always populated by a service layer method
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
        setNumericPrice(new BigDecimal(price));
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getNumericPrice() {
        return numericPrice;
    }

    public void setNumericPrice(BigDecimal numericPrice) {
        this.numericPrice = numericPrice;
    }

    @Override
    public String toString() {
        return "{bookName: " + bookName + "|"
                + "price: " + price + "|"
                + "amount: " + amount + "|"
                + "orderId: " + orderId + "}";
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Order) {
            Order comp = (Order) obj;
            return comp.orderId.equals(this.orderId);
        }
        return false;
    }


    @Override
    public int compareTo(Object o) {
        Order order = (Order) o;
        return getNumericPrice().compareTo(order.numericPrice);
    }
}
