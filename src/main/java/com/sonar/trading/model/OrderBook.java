package com.sonar.trading.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderBook {

    private String success;
    @JsonProperty("payload")
    private OrderBookPayload payload;

    public OrderBook() {
        // Always populated by a service layer method
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public OrderBookPayload getPayload() {
        return payload;
    }

    public void setPayload(OrderBookPayload payload) {
        this.payload = payload;
    }

    public String toString() {
        return "OrderBook{success: " + success + "|"
                + "OrderBookPayload: " + payload.toString() + "}";
    }
}
