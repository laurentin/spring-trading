package com.sonar.trading.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class OrderBookPayload {

    @JsonProperty("updated_at")
    private String updatedAt;
    private List<Order> bids;
    private List<Order> asks;
    private String sequence;

    public OrderBookPayload() {
        // Always populated by a service layer method
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Order> getBids() {
        return bids;
    }

    public void setBids(List<Order> bids) {
        this.bids = bids;
    }

    public List<Order> getAsks() {
        return asks;
    }

    public void setAsks(List<Order> asks) {
        this.asks = asks;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    @Override
    public String toString() {
        return "{updatedAt: " + updatedAt + "|"
                + "bids length: " + bids.size() + "|"
                + "asks length: " + asks.size() + "|"
                + "bids: " + bids + "|"
                + "asks: " + asks + "|"
                + "sequence: " + sequence + "}";
    }
}