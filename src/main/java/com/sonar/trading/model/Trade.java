package com.sonar.trading.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Trade {

    @JsonProperty("book")
    private String bookName;
    @JsonProperty("created_at")
    private String createdAt;
    private String amount;
    @JsonProperty("maker_side")
    private String makerSide;
    private String price;
    @JsonProperty("tid")
    private String transactionId;

    public Trade() {
        // Always populated by a service layer method
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMakerSide() {
        return makerSide;
    }

    public void setMakerSide(String makerSide) {
        this.makerSide = makerSide;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String toString() {
        return "{bookName: " + bookName + "|"
                + "price: " + price + "|"
                + "amount: " + amount + "|"
                + "tid: " + transactionId + "|"
                + "makerSide: " + makerSide + "|"
                + "createdAt: " + createdAt + "}";
    }
}
