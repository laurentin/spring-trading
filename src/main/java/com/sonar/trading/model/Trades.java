package com.sonar.trading.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Trades {

    private String success;
    @JsonProperty("payload")
    private List<Trade> tradeList;

    public Trades() {
        // Always populated by a service layer method
    }

    public Trades(String success, List<Trade> tradeList) {
        this.success = success;
        this.tradeList = tradeList;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<Trade> getTradeList() {
        return tradeList;
    }

    public void setTradeList(List<Trade> tradeList) {
        this.tradeList = tradeList;
    }

    @Override
    public String toString() {
        return "Trades{success: " + success + "|"
                + "tradeList: " + tradeList + "}";
    }
}
