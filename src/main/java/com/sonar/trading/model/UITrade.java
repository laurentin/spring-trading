package com.sonar.trading.model;

public class UITrade {

    private String created_at;
    private String maker_side;
    private String price;
    private String amount;
    private String tid;
    private String highlight;

    public UITrade() {
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getMaker_side() {
        return maker_side;
    }

    public void setMaker_side(String maker_side) {
        this.maker_side = maker_side;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getHighlight() {
        return highlight;
    }

    public void setHighlight(String highlight) {
        this.highlight = highlight;
    }
}
