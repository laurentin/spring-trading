package com.sonar.trading.service;

import com.sonar.trading.model.OrderBook;

public interface BookSnapshotService {

    OrderBook getBookSnapshot(String bookName);

}
