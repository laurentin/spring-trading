package com.sonar.trading.service;

import com.sonar.trading.model.OrderBook;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
@Order(1)
public class BookSnapshotServiceImpl implements BookSnapshotService {
    private static final Logger log = LoggerFactory.getLogger(BookSnapshotServiceImpl.class);

    @Autowired
    OrderService orderService;

    @Override
    public OrderBook getBookSnapshot(String bookName) {
        log.info("Getting order book snapshot for book name: {}", bookName);
        OrderBook orderBook = new OrderBook();
        try {
            CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();
            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
            requestFactory.setHttpClient(httpClient);
            RestTemplate restTemplate = new RestTemplate(requestFactory);
            orderBook = restTemplate.getForObject("https://api.bitso.com/v3/order_book/?book="+bookName+"&aggregate=false", OrderBook.class);
            log.info("getBookSnapshot: {}", orderBook);
            orderService.initializeOrderBook(orderBook);

        } catch(HttpClientErrorException httpClientErrorException) {
            log.error("Error while calling BITSO - GET order_book endpoint: {}", httpClientErrorException.getMessage());
        }
        return orderBook;
    }
}
