package com.sonar.trading.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
public class OrderProcessorExecutor {

    @Value("${order.thread.core-pool}")
    private int corePoolSize;

    @Value("${order.thread.max-pool}")
    private int maxPoolSize;

    @Value("${order.queue.capacity}")
    private int queueCapacity;

    @Bean
    @Qualifier("orderProcessorExecutor")
    public Executor asyncExecutor(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setThreadNamePrefix("Order-Proc-");
        executor.initialize();
        return executor;
    }

}
