package com.sonar.trading.service;

import com.sonar.trading.model.BestOrder;
import com.sonar.trading.model.DiffOrder;
import com.sonar.trading.model.OrderBook;

import java.util.concurrent.CompletableFuture;

public interface OrderService {

    void initializeOrderBook(OrderBook orderBook);
    CompletableFuture<Integer> asyncOrderProcessor(DiffOrder diffOrder);
    BestOrder getBestOrders(int sizeX);


}
