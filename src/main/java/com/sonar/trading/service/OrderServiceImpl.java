package com.sonar.trading.service;

import com.sonar.trading.model.BestOrder;
import com.sonar.trading.model.DiffOrder;
import com.sonar.trading.model.DiffOrderDetail;
import com.sonar.trading.model.Ledger;
import com.sonar.trading.model.Order;
import com.sonar.trading.model.OrderBook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

import static com.sonar.trading.util.Constants.BOOK_NAME;
import static com.sonar.trading.util.Constants.BUY;
import static com.sonar.trading.util.Constants.CANCELLED;
import static com.sonar.trading.util.Constants.COMPLETED;
import static com.sonar.trading.util.Constants.KA;
import static com.sonar.trading.util.Constants.OPEN;
import static com.sonar.trading.util.Constants.SELL;

@Service
public class OrderServiceImpl implements OrderService {

    private static final Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);
    private static Ledger ledger = new Ledger();
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    @Override
    public void initializeOrderBook(OrderBook orderBook) {
        log.info("Initializing in-memory order book snapshot");
        List<Order> bids = new LinkedList<>(orderBook.getPayload().getBids());
        List<Order> asks = new LinkedList<>(orderBook.getPayload().getAsks());
        ledger.setBids(bids);
        ledger.setAsks(asks);
        ledger.setSequence(new BigDecimal(orderBook.getPayload().getSequence()));
        ledger.setUpdateAt(orderBook.getPayload().getUpdatedAt());
        log.info("In-memory order book snapshot initialized with {} bids and {} asks", bids.size(), asks.size());
    }


    @Override
    @Async("orderProcessorExecutor")
    public CompletableFuture<Integer> asyncOrderProcessor(DiffOrder diffOrder) {
        CompletableFuture<Integer> buysProcessed;
        CompletableFuture<Integer> sellsProcessed;
        Integer totalProcessed = 0;
        log.info("Received: {}", diffOrder);
        if (diffOrder!=null && !diffOrder.getType().equalsIgnoreCase(KA)) {
            try {
                if (CollectionUtils.isEmpty(ledger.getBids()) || CollectionUtils.isEmpty(ledger.getAsks())) {
                    // Wait till ledger has been initialized
                    TimeUnit.SECONDS.sleep(5);
                }
                List<DiffOrderDetail> buys = diffOrder.getDiffOrderDetail().stream().parallel()
                        .filter(detail -> detail.getOrderType() == BUY).collect(Collectors.toList());

                List<DiffOrderDetail> sells = diffOrder.getDiffOrderDetail().stream().parallel()
                        .filter(detail -> detail.getOrderType() == SELL).collect(Collectors.toList());

                // Playback the queued message, discarding the ones with sequence number below or equal
                // to the one from the snapshot order book (only process when orderSeq > ledgerSeq)
                if (diffOrder.getSequence().compareTo(ledger.getSequence()) > 0) {
                    log.info("Processing orders. Order Seq {} > Ledger Seq {}", diffOrder.getSequence(),
                            ledger.getSequence());
                    buysProcessed = asyncBuysProcessor(buys);
                    sellsProcessed = asyncSellsProcessor(sells);
                    CompletableFuture.allOf(buysProcessed, sellsProcessed);
                    totalProcessed = buysProcessed.get() + sellsProcessed.get();
                } else {
                    log.info("Discarding obsolete orders. Order Seq {} <= Ledger Seq {}", diffOrder.getSequence(),
                            ledger.getSequence());
                }
            } catch (Exception e) {
                log.error("Error while asyncOrderProcessor: {}", e.getMessage());
            }
        } else {
            log.info("Skipping KA message");
        }
        return CompletableFuture.completedFuture(totalProcessed);
    }


    @Async("buysOrderProcessorExec")
    public CompletableFuture<Integer> asyncBuysProcessor(List<DiffOrderDetail> buys) {
        log.info("Buys Received: {}", buys.size());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
        buys.parallelStream().forEach(diffOrderDetail -> {
            log.info("Bid Order: {}", diffOrderDetail);
            boolean orderExist = (findBid(diffOrderDetail.getOrderId())!=null);
            // Order cancellation -> remove
            if (diffOrderDetail.getOrderAmount()==null &&
                    diffOrderDetail.getOrderStatus().equalsIgnoreCase(CANCELLED)) {
                removeBid(diffOrderDetail.getOrderId());

            // Order open -> update
            } else if (diffOrderDetail.getOrderStatus().equalsIgnoreCase(OPEN) && orderExist) {
                updateBid(diffOrderDetail);

            // Order completed -> remove
            } else if (diffOrderDetail.getOrderStatus().equalsIgnoreCase(COMPLETED) && orderExist) {
                removeBid(diffOrderDetail.getOrderId());

            // Order not found -> add
            } else if (!orderExist) {
                addBid(diffOrderDetail);
            }
            lock.writeLock().lock();
            try {
                 String updatedTimeStamp = dateFormat.format(new Date(diffOrderDetail.getOrderTimestamp().longValue()));
                ledger.setUpdateAt(updatedTimeStamp);
            } finally {
                lock.writeLock().unlock();
            }

        });
        return CompletableFuture.completedFuture(buys.size());
    }


    private Order findBid(String oid) {
        Order orderOut = null;
        try {
            orderOut = ledger.getBids().stream().parallel().filter(order -> order.getOrderId()
                    .equalsIgnoreCase(oid)).findAny().orElse(null);
            if (orderOut!=null) {
                log.info("**Bid found: {} with index: {}", orderOut, findBidIndex(orderOut));
            } else {
                log.info("**Bid not found: {}", oid);
            }
        } catch (Exception e) {
            log.info("Bid Order {} does not exist, {}", oid, e.getMessage());
        }
        return orderOut;
    }


    private void updateBid(DiffOrderDetail diffOrderDetail) {
        log.info("updateBid: {}", diffOrderDetail.getOrderId());
        Order order = mapperToOrder(diffOrderDetail);
        synchronized(this) {
            int index = findBidIndex(order);
            ledger.getBids().set(index, order);
        }
    }


    private void addBid(DiffOrderDetail diffOrderDetail) {
        log.info("addBid: {}", diffOrderDetail.getOrderId());
        synchronized(this) {
            ledger.getBids().add(mapperToOrder(diffOrderDetail));
        }
    }


    private int findBidIndex(Order order) {
        return ledger.getBids().indexOf(order);
    }


    private void removeBid(String orderId) {
        log.info("removeBid: {}", orderId);
        synchronized(this) {
            ledger.getBids().removeIf(order -> order.getOrderId().equals(orderId));
        }
    }


    @Async("sellsOrderProcessorExec")
    public CompletableFuture<Integer> asyncSellsProcessor(List<DiffOrderDetail> sells) {
        log.info("Sells Received: {}", sells.size());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
        sells.parallelStream().forEach(diffOrderDetail -> {
            log.info("Ask Order: {}", diffOrderDetail);
            boolean orderExist = (findAsk(diffOrderDetail.getOrderId())!=null);
            // Order cancellation -> remove
            if (diffOrderDetail.getOrderAmount()==null &&
                    diffOrderDetail.getOrderStatus().equalsIgnoreCase(CANCELLED)) {
                removeAsk(diffOrderDetail.getOrderId());

                // Order open -> update
            } else if (diffOrderDetail.getOrderStatus().equalsIgnoreCase(OPEN) && orderExist) {
                updateAsk(diffOrderDetail);

                // Order completed -> remove
            } else if (diffOrderDetail.getOrderStatus().equalsIgnoreCase(COMPLETED) && orderExist) {
                removeAsk(diffOrderDetail.getOrderId());

                // Order not found -> add
            } else if (!orderExist) {
                addAsk(diffOrderDetail);
            }
            lock.writeLock().lock();
            try {
                String updatedTimeStamp = dateFormat.format(new Date(diffOrderDetail.getOrderTimestamp().longValue()));
                ledger.setUpdateAt(updatedTimeStamp);
            } finally {
                lock.writeLock().unlock();
            }
        });

        return CompletableFuture.completedFuture(sells.size());
    }


    private Order findAsk(String oid) {
        Order orderOut = null;
        try {
            orderOut = ledger.getAsks().stream().parallel().filter(order -> order.getOrderId()
                    .equalsIgnoreCase(oid)).findAny().orElse(null);
            if (orderOut!=null) {
                log.info("**Ask found: {} with index: {}", orderOut, findAskIndex(orderOut));
            } else {
                log.info("**Ask not found: {}", oid);
            }
        } catch (Exception e) {
            log.info("**Ask Order {} does not exist, {}", oid, e.getMessage());
        }
        return orderOut;
    }


    private void updateAsk(DiffOrderDetail diffOrderDetail) {
        log.info("updateAsk: {}", diffOrderDetail.getOrderId());
        Order order = mapperToOrder(diffOrderDetail);
        int index = findAskIndex(order);
        synchronized(this) {
            ledger.getAsks().set(index, order);
        }
    }


    private void addAsk(DiffOrderDetail diffOrderDetail) {
        log.info("addAsk: {}", diffOrderDetail.getOrderId());
        synchronized(this) {
            ledger.getAsks().add(mapperToOrder(diffOrderDetail));
        }
    }


    private int findAskIndex(Order order) {
        return ledger.getAsks().indexOf(order);
    }


    private void removeAsk(String orderId) {
        log.info("removeAsk: {}", orderId);
        ledger.getAsks().removeIf(order -> order.getOrderId().equals(orderId));
    }


    private Order mapperToOrder(DiffOrderDetail diffOrderDetail) {
        Order order = new Order();
        order.setAmount(diffOrderDetail.getOrderAmount());
        order.setOrderId(diffOrderDetail.getOrderId());
        order.setPrice(diffOrderDetail.getOrderValue());
        order.setBookName(BOOK_NAME);
        return order;
    }


    public BestOrder getBestOrders(int sizeX) {
        BestOrder bestOrder = new BestOrder();
        try {
            bestOrder.setUpdated_at(ledger.getUpdateAt());
            List<Order> bestAsks = new ArrayList<>(ledger.getAsks());
            List<Order> bestBids = new ArrayList<>(ledger.getBids());

            // Sorting output collections
            Collections.sort(bestAsks);
            Collections.sort(bestBids, Collections.reverseOrder());

            // Use config option X to limit number of ASKs / BIDs displayed in UI.
            if (sizeX <= bestAsks.size()) {
                bestAsks = bestAsks.subList(0, sizeX - 1);
            }
            if (sizeX <= bestBids.size()) {
                bestBids = bestBids.subList(0, sizeX - 1);
            }
            bestOrder.setAsks(bestAsks);
            bestOrder.setBids(bestBids);
        }catch (Exception e) {
            log.error("Error while loading best orders: {}", e.getMessage());
        }
        return bestOrder;

    }

}
