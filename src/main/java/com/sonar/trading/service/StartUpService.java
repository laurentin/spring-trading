package com.sonar.trading.service;

public interface StartUpService {

    void startSonarTradingFramework();

}
