package com.sonar.trading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.sonar.trading.util.DiffOrderHelper.subscribeDiffOrderChannel;
import static com.sonar.trading.util.Constants.BOOK_NAME;

@Service
public class StartUpServiceImpl implements StartUpService {

    @Autowired
    BookSnapshotService bookSnapshotService;

    @Autowired
    TradesServices tradesServices;


    @Override
    public void startSonarTradingFramework() {
        subscribeDiffOrderChannel();
        bookSnapshotService.getBookSnapshot(BOOK_NAME);
        tradesServices.startTradesScheduler(BOOK_NAME);

    }
}
