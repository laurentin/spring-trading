package com.sonar.trading.service;

import com.sonar.trading.model.UITrade;
import com.sonar.trading.model.Trades;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface TradesServices {

    void startTradesScheduler(String bookName);
    Trades getTrades(String bookName);
    List<UITrade> getRecentTrades();
    void startTradesPooling();
    CompletableFuture<Integer> asyncContrarianProcessor();

}
