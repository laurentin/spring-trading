package com.sonar.trading.service;

import com.sonar.trading.model.UITrade;
import com.sonar.trading.model.ContrarianState;
import com.sonar.trading.model.Trade;
import com.sonar.trading.model.Trades;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

import static com.sonar.trading.util.Constants.BOOK_NAME;
import static com.sonar.trading.util.Constants.MARKER_BUY;
import static com.sonar.trading.util.Constants.MARKER_SELL;

@Service
public class TradesServicesImpl implements TradesServices {

    private static final Logger log = LoggerFactory.getLogger(TradesServicesImpl.class);
    private static Trades tradesBook = new Trades("true",new LinkedList<>());

    private ReadWriteLock lock = new ReentrantReadWriteLock();

    // Use config option X
    @Value("${X.UI}")
    private int size;

    // Use config option M - due Bitso limit of 100 trades per request
    // this value have to be less than 100.
    @Value("${M.UP}")
    private int M;

    // Use config option N - due Bitso limit of 100 trades per request
    // this value have to be less than 100.
    @Value("${N.DW}")
    private int N;

    @Override
    public void startTradesScheduler(String bookName) {
        log.info("Starting Trades scheduler for book: {}", bookName);
        startTradesPooling();
    }


    // Schedule the polling of trades over REST
    @Scheduled(fixedRate = 30000)
    public void startTradesPooling() {
        log.info("Pooling trades every 30 secs...");
        Trades trades = getTrades(BOOK_NAME);
        log.info("Got {} trades.", trades.getTradeList().size());
    }


    @Override
    public Trades getTrades(String bookName) {
        log.info("Poll for recent Trades of book: {}", bookName);
        CompletableFuture<Integer> tradesProcessed;
        Trades trades = new Trades();
        try {
            CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();
            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
            requestFactory.setHttpClient(httpClient);
            RestTemplate restTemplate = new RestTemplate(requestFactory);
            // Use config option X to request recent trades
            int limit = size;
            // BITSO GET trades has a max limit of 100.
            if (size > 100) {
                limit = 100;
            }
            trades = restTemplate.getForObject("https://api.bitso.com/v3/trades/?book="+bookName+"&limit="+limit, Trades.class);
            lock.writeLock().lock();
            try {
                tradesBook.setSuccess(trades.getSuccess());
                tradesBook.setTradeList(trades.getTradeList());
            } finally {
                lock.writeLock().unlock();
            }
            log.info("getTrades - recent Trades: {} - {}", tradesBook.getTradeList().size(), tradesBook);

            // The loop that causes the trading algorithm to reevaluate
            tradesProcessed = asyncContrarianProcessor();
            log.info("Imaginary decisions made: {}", tradesProcessed.get());

        } catch(HttpClientErrorException httpClientErrorException) {
            log.error("Error while calling BITSO - GET trades endpoint: {}", httpClientErrorException.getMessage());

        } catch(Exception e) {
            log.error("Error while processing contrarian strategy");
        }
        return trades;
    }

    @Async("orderProcessorExecutor")
    public CompletableFuture<Integer> asyncContrarianProcessor() {
        List<Trade> tradeListCopy;
        List<Trade> sells;
        List<Trade> buys;
        lock.readLock().lock();
        try {
            tradeListCopy = new LinkedList<>(tradesBook.getTradeList());
        } finally {
            lock.readLock().unlock();
        }
        sells = tradeListCopy.stream()
                .parallel().filter(t -> t.getMakerSide().equalsIgnoreCase(MARKER_SELL))
                .collect(Collectors.toList());

        buys = tradeListCopy.stream()
                .parallel().filter(t -> t.getMakerSide().equalsIgnoreCase(MARKER_BUY))
                .collect(Collectors.toList());
        log.info("SELL TRADES: {}", sells);
        log.info("BUY TRADES: {}", buys);
        log.info("Trades processed: {}", sells.size() + buys.size());
        return CompletableFuture.completedFuture(checkAndTradeOnUpticks(sells, M)
                + checkAndTradeOnDownticks(buys, N));
    }


    public Integer checkAndTradeOnUpticks(List<Trade> sells, int count) {
        ContrarianState state = new ContrarianState(new BigDecimal(0),0,0);
        Integer decisionsMade = 0;
        BigDecimal tradePrice;
        for (Trade trade: sells) {
            tradePrice = new BigDecimal(trade.getPrice());
            if (state.getPrice().compareTo(tradePrice) == 0) {
                // ZERO TICK
                log.info("CTR-STRATEGY -> ZERO TICK FOUND");

            } else if (state.getPrice().compareTo(tradePrice) < 0) {
                // UP TICK
                log.info("CTR-STRATEGY -> UP TICK FOUND");
                state.setUpticks(state.getUpticks()+1);
                state.setDownticks(0);
                if (state.getUpticks()==count) {
                    // SELL 1 BTC
                    log.info("CTR-STRATEGY -> SELL 1BTC");
                    tradeBitCoin(generateTrade(trade, MARKER_SELL));
                    state.setUpticks(0);
                    decisionsMade++;
                }
            }
            state.setPrice(tradePrice);
        }
        return decisionsMade;
    }


    public Integer checkAndTradeOnDownticks(List<Trade> buys, int count) {
        ContrarianState state = new ContrarianState(new BigDecimal(0),0,0);
        Integer decisionsMade = 0;
        BigDecimal tradePrice;
        for (Trade trade: buys) {
            tradePrice = new BigDecimal(trade.getPrice());
            if (state.getPrice().compareTo(tradePrice) == 0) {
                // ZERO TICK
                log.info("CTR-STRATEGY -> ZERO TICK FOUND");

            } else if (state.getPrice().compareTo(tradePrice) > 0) {
                // DOWN TICK
                log.info("CTR-STRATEGY -> DOWN TICK FOUND");
                state.setDownticks(state.getDownticks() + 1);
                state.setUpticks(0);
                if (state.getDownticks() == count) {
                    // BUY 1 BTC
                    log.info("CTR-STRATEGY -> BUY 1BTC");
                    tradeBitCoin(generateTrade(trade, MARKER_BUY));
                    state.setDownticks(0);
                    decisionsMade++;
                }
            }
            state.setPrice(tradePrice);
        }
        return decisionsMade;
    }


    private void tradeBitCoin(Trade newTrade) {
        // inject the new trade at the top position of Trades book
        // Asks asc, bids desc
        lock.writeLock().lock();
        try {
            tradesBook.getTradeList().add(0,newTrade);
        } finally {
            lock.writeLock().unlock();
        }
        log.info("Injected imaginary Trade: {}", newTrade);
        log.info("Trades book updated: {}", tradesBook.getTradeList());
    }


    private static Trade generateTrade(Trade latestTrade, String makerSide) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
        String updatedTimeStamp = dateFormat.format(new Date());
        Trade newTrade = new Trade();
        newTrade.setTransactionId(UUID.randomUUID().toString().substring(0,13));
        newTrade.setAmount("1");
        newTrade.setBookName(latestTrade.getBookName());
        newTrade.setMakerSide(makerSide);
        newTrade.setPrice(latestTrade.getPrice());
        newTrade.setCreatedAt(updatedTimeStamp);
        log.info("Imaginary Trade generated: {}", newTrade);
        return  newTrade;
    }


    @Override
    public List<UITrade> getRecentTrades() {
        List<Trade> tradeList;
        List<UITrade> UITradeList = new ArrayList<>();

        lock.readLock().lock();
        try {
            tradeList = new ArrayList<>(tradesBook.getTradeList());
        } finally {
            lock.readLock().unlock();
        }

        // Use config option X to limit number of recent trades displayed in UI.
        if (size <= tradeList.size()) {
            tradeList = tradeList.subList(0, size-1);
        }
        tradeList.forEach(trade -> UITradeList.add(mapperToUITrade(trade)));
        return UITradeList;
    }


    // mapper to make a match with the original UI's contract
    private UITrade mapperToUITrade(Trade trade) {
        UITrade UITrade = new UITrade();
        UITrade.setAmount(trade.getAmount());
        UITrade.setCreated_at(trade.getCreatedAt());
        try {
            // check if tid is a number or not
            Long.valueOf(trade.getTransactionId());
            UITrade.setHighlight("0");
        } catch(Exception e){
            // Imaginary Trade
            UITrade.setHighlight("1");
        }
        UITrade.setMaker_side(trade.getMakerSide());
        UITrade.setPrice(trade.getPrice());
        UITrade.setTid(trade.getTransactionId());
        return UITrade;
    }
}
