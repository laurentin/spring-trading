package com.sonar.trading.util;

public final class Constants {

    private Constants() {}
    public static final String BOOK_NAME = "btc_mxn";
    public static final String BITSO_URL="wss://ws.bitso.com";
    public static final String ACTION_SUBSCRIBE_BOOK_BTC_MXN_DIFF_ORDERS = "{\"action\": \"subscribe\",\"book\": \"btc_mxn\",\"type\": \"diff-orders\"}";
    public static final int BUY = 0;
    public static final int SELL = 1;
    public static final String CANCELLED = "cancelled";
    public static final String OPEN = "open";
    public static final String COMPLETED = "completed";
    public static final String KA = "ka";
    public static final String MARKER_BUY = "buy";
    public static final String MARKER_SELL = "sell";

}
