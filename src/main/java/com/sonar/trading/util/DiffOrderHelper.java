package com.sonar.trading.util;

import com.sonar.trading.websocket.DiffOrderClient;

import static com.sonar.trading.util.Constants.ACTION_SUBSCRIBE_BOOK_BTC_MXN_DIFF_ORDERS;


public class DiffOrderHelper {

    private DiffOrderHelper() {}

    public static void subscribeDiffOrderChannel() {
        DiffOrderClient diffOrderClient = new DiffOrderClient();
        diffOrderClient.sendMessage(ACTION_SUBSCRIBE_BOOK_BTC_MXN_DIFF_ORDERS);
    }


}
