package com.sonar.trading.websocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sonar.trading.model.DiffOrder;
import com.sonar.trading.service.OrderService;
import com.sonar.trading.service.OrderServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.CompletableFuture;

import javax.websocket.ClientEndpoint;
import javax.websocket.ContainerProvider;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import static com.sonar.trading.util.Constants.BITSO_URL;

@ClientEndpoint
public class DiffOrderClient {

    private static final Logger log = LoggerFactory.getLogger(DiffOrderClient.class);
    private Session session;

    private static OrderService orderService = new OrderServiceImpl();


    public DiffOrderClient() {
        try{
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this, new URI(BITSO_URL));

        } catch(Exception ex){
            log.error(ex.getMessage());
        }
    }


    @OnOpen
    public void onOpen(Session session){
        this.session=session;
    }


    @OnMessage
    @Async("orderProcessorExecutor")
    public void onMessage(String message, Session session){
        DiffOrder diffOrder;
        try {
            diffOrder = new ObjectMapper().readValue(message, DiffOrder.class);
            CompletableFuture<Integer> ordersProcessed = orderService.asyncOrderProcessor(diffOrder);
            log.info("Orders processed: {}", ordersProcessed.get());
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }

    }


    public void sendMessage(String message){
        try {
            session.getBasicRemote().sendText(message);
        } catch (IOException ex) {
            log.error(ex.getMessage());
        }
    }

}
