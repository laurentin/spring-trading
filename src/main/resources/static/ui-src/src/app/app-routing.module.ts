import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'best-open-orders', pathMatch: 'full' },
  {
    path: 'best-open-orders',
    loadChildren: './open-orders/open-orders.module#OpenOrdersModule',
  },
  {
    path: 'latest-trades',
    loadChildren: './trades/trades.module#TradesModule',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
