import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { OrderService } from './services/order.service';
import { TradeService } from './services/trade.service';

@NgModule({
  imports: [CommonModule, HttpClientModule],
  declarations: [],
  providers: [OrderService, TradeService],
})
export class CoreModule {}
