export interface Point {
  x: string | number;
  y: string | number;
}
