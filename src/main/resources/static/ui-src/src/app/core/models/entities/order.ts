export interface Order {
  bids: Price[];
  asks: Price[];
  updated_at: string;
}

export interface Price {
  price: string;
  amount: string;
  oid: string;
}
