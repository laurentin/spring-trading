export interface Trade {
  created_at: string;

  /**
    * Transaction type - "sell" or "buy"
    */
  maker_side: 'sell' | 'buy';

  /**
    * Price in Mexican Peso
    */
  price: string;

  /**
    * Unique transaction id
    */
  amount: string; // bitcoin amount

  /**
    * Unique transaction id
    */
  tid: number;

  /**
    * To differentiate real from imaginary trades – 0 or 1 (1 means is imaginary)
    */
  highlight: '0' | '1';
}
