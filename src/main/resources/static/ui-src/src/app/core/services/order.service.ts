import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, interval, empty, of } from 'rxjs';
import { switchMap, startWith, catchError } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { Order } from '../models/entities/order';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  orders$: Observable<Order>;

  private urlApi: string;

  constructor(private http: HttpClient) {
    this.urlApi = `${environment.host}/orders`;
    this.orders$ = interval(environment.refreshTime).pipe(
      startWith(0),
      switchMap(_ =>
        this.getBest().pipe(
          this.valid(),
          catchError(__ => empty()),
        ),
      ),
    );
  }

  private getBest() {
    return this.http.get<Order>(`${this.urlApi}/best`);
  }

  private valid() {
    return (source: Observable<Order>) =>
      source.pipe(
        switchMap(
          o => (!o || !o.asks || !o.bids || !o.updated_at ? empty() : of(o)),
        ),
      );
  }
}
