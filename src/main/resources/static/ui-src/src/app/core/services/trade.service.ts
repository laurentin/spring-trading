import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, interval, empty, of } from 'rxjs';
import { startWith, switchMap, catchError } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { Trade } from '../models/entities/trade';

@Injectable({
  providedIn: 'root',
})
export class TradeService {
  trades$: Observable<Trade[]>;

  private urlApi: string;

  constructor(private http: HttpClient) {
    this.urlApi = `${environment.host}/trades`;
    this.trades$ = interval(environment.refreshTime).pipe(
      startWith(0),
      switchMap(_ =>
        this.getLatest().pipe(
          this.valid(),
          catchError(__ => empty()),
        ),
      ),
    );
  }

  private getLatest() {
    return this.http.get<Trade[]>(`${this.urlApi}/latest`);
  }

  private valid() {
    return (source: Observable<Trade[]>) =>
      source.pipe(switchMap(t => (!t ? empty() : of(t))));
  }
}
