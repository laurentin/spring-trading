import {
  Component,
  OnInit,
  ViewChild,
  Input,
  OnDestroy,
  ChangeDetectionStrategy,
} from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { OrderTableDataSource } from './order-table-datasource';
import { Order, Price } from '../../../core/models/entities/order';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-order-table',
  templateUrl: './order-table.component.html',
  styleUrls: ['./order-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderTableComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  @ViewChild(MatSort)
  sort: MatSort;

  dataSource: OrderTableDataSource;
  dataSource$ = new BehaviorSubject<{ ask: Price; bid: Price }[]>([]);

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['bidPrice', 'askPrice'];

  @Input()
  public get order(): Order {
    return this._order;
  }
  public set order(value: Order) {
    this._order = value;
    this.dataSource$.next(
      (value && value.asks.map((a, i) => ({ ask: a, bid: value.bids[i] }))) ||
        [],
    );
  }

  private _order: Order;

  private destroy$ = new Subject();

  ngOnInit() {
    this.dataSource = new OrderTableDataSource(this.paginator, this.sort);
    this.dataSource$.pipe(takeUntil(this.destroy$)).subscribe(data => {
      this.dataSource = new OrderTableDataSource(this.paginator, this.sort);
      this.dataSource.data = data;
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
