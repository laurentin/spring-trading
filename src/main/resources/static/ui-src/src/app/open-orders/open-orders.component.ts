import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ChangeDetectionStrategy,
} from '@angular/core';

import { BehaviorSubject, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import ApexCharts from 'apexcharts';

import { Order } from '../core/models/entities/order';
import { OrderService } from '../core/services/order.service';

@Component({
  selector: 'app-open-orders',
  templateUrl: './open-orders.component.html',
  styleUrls: ['./open-orders.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OpenOrdersComponent implements AfterViewInit, OnInit, OnDestroy {
  order$ = new BehaviorSubject<Partial<Order>>({
    asks: [],
    bids: [],
  });
  @ViewChild('chart')
  chart: ElementRef;

  private destroy$ = new Subject();

  constructor(private orderService: OrderService) {}

  ngAfterViewInit() {
    const options = {
      chart: {
        height: 350,
        type: 'line',
        animations: {
          enabled: true,
          easing: 'linear',
          dynamicAnimation: {
            speed: 1000,
          },
        },
        foreColor: '#ccc',
        toolbar: {
          show: true,
        },
        zoom: {
          enabled: true,
          type: 'y',
        },
      },
      tooltip: {
        theme: 'dark',
      },
      grid: {
        borderColor: '#535A6C',
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: 'smooth',
      },
      series: [],
      markers: {
        size: 0,
      },
      legend: {
        position: 'bottom',
        horizontalAlign: 'left',
      },
    };
    const orderChart = new ApexCharts(this.chart.nativeElement, options);
    orderChart.render();
    this.order$
      .pipe(
        map(o => ({
          asks: o.asks.map((a, i) => ({ x: i, y: Number(a.price) })),
          bids: o.bids.map((b, i) => ({ x: i, y: Number(b.price) })),
        })),
        takeUntil(this.destroy$),
      )
      .subscribe(({ asks, bids }) => {
        orderChart.updateSeries([
          { data: bids, name: 'bids' },
          { data: asks, name: 'asks' },
        ]);
      });
  }

  ngOnInit() {
    this.orderService.orders$
      .pipe(takeUntil(this.destroy$))
      .subscribe(order => {
        this.order$.next(order);
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
