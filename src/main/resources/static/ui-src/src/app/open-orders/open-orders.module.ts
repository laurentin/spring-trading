import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import {
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
} from '@angular/material';

import { OpenOrdersComponent } from './open-orders.component';
import { OrderTableComponent } from './components/order-table/order-table.component';

const routes: Routes = [{ path: '', component: OpenOrdersComponent }];

@NgModule({
  declarations: [OpenOrdersComponent, OrderTableComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
  ],
  providers: [],
})
export class OpenOrdersModule {}
