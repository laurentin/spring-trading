import { Component, OnInit, ViewChild, Input, OnDestroy } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { TradeTableDataSource } from './trade-table-datasource';
import { Trade } from '../../../core/models/entities/trade';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-trade-table',
  templateUrl: './trade-table.component.html',
  styleUrls: ['./trade-table.component.scss'],
})
export class TradeTableComponent implements OnInit, OnDestroy {
  private _trades: Trade[];

  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  @ViewChild(MatSort)
  sort: MatSort;

  dataSource: TradeTableDataSource;
  dataSource$ = new BehaviorSubject<{ sell: Trade; buy: Trade }[]>([]);

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['buyPrice', 'sellPrice'];

  @Input()
  public get trades(): Trade[] {
    return this._trades;
  }
  public set trades(trades: Trade[]) {
    this._trades = trades || [];

    const flatTrades = {
      buy: this._trades.filter(t => t.maker_side === 'buy'),
      sell: this._trades.filter(t => t.maker_side === 'sell'),
    };

    const length = Math.max(flatTrades.sell.length, flatTrades.buy.length);
    const source = [];
    for (let i = 0; i < length; i++) {
      source.push({
        sell: flatTrades.sell[i] || {},
        buy: flatTrades.buy[i] || {},
      });
    }
    this.dataSource$.next(source);
  }
  private destroy$ = new Subject();

  ngOnInit() {
    this.dataSource = new TradeTableDataSource(this.paginator, this.sort);
    this.dataSource$.pipe(takeUntil(this.destroy$)).subscribe(data => {
      this.dataSource = new TradeTableDataSource(this.paginator, this.sort);
      this.dataSource.data = data;
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
