import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ChangeDetectionStrategy,
} from '@angular/core';

import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil, map, tap } from 'rxjs/operators';
import ApexCharts from 'apexcharts';

import { TradeService } from '../core/services/trade.service';
import { Trade } from '../core/models/entities/trade';

@Component({
  selector: 'app-trades',
  templateUrl: './trades.component.html',
  styleUrls: ['./trades.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TradesComponent implements AfterViewInit, OnInit, OnDestroy {
  trades$ = new BehaviorSubject<Partial<Trade[]>>([]);

  @ViewChild('chart')
  chart: ElementRef;

  private destroy$ = new Subject();

  constructor(private tradeService: TradeService) {}

  ngAfterViewInit() {
    const options = {
      chart: {
        height: 350,
        type: 'line',
        animations: {
          enabled: true,
          easing: 'linear',
          dynamicAnimation: {
            speed: 1000,
          },
        },
        foreColor: '#ccc',
        toolbar: {
          show: true,
        },
        zoom: {
          enabled: true,
          type: 'y',
        },
      },
      tooltip: {
        theme: 'dark',
      },
      grid: {
        borderColor: '#535A6C',
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: 'smooth',
      },
      series: [],
      markers: {
        size: 0,
      },
      legend: {
        position: 'bottom',
        horizontalAlign: 'left',
      },
      // xaxis: {
      //   type: 'datetime',
      // },
    };
    const tradeChart = new ApexCharts(this.chart.nativeElement, options);
    tradeChart.render();
    this.trades$
      .pipe(
        map(trades => ({
          buy: trades.filter(t => t.maker_side === 'buy'),
          sell: trades.filter(t => t.maker_side === 'sell'),
        })),
        map(trades => {
          const points = {
            buy: trades.buy.map((t, i) => ({
              x: i,
              y: Number(t.price),
              highlight: t.highlight,
              maker_side: t.maker_side,
            })),
            sell: trades.sell.map((t, i) => ({
              x: i,
              y: Number(t.price),
              highlight: t.highlight,
              maker_side: t.maker_side,
            })),
          };

          const annotations = [...points.buy, ...points.sell]
            .filter(t => t.highlight === '1')
            .map(t => ({
              x: t.x,
              y: t.y,
              marker: {
                size: 6,
                fillColor: '#fff',
                strokeColor: t.maker_side === 'buy' ? '#2698FF' : '#26E7A6',
                radius: 2,
              },
            }));

          return { ...points, annotations };
        }),
        takeUntil(this.destroy$),
      )
      .subscribe(({ buy, sell, annotations }) => {
        tradeChart.updateSeries([
          { data: buy, name: 'Buy' },
          { data: sell, name: 'Sell' },
        ]);

        tradeChart.updateOptions({ annotations: { points: annotations } });
      });
  }

  ngOnInit() {
    this.tradeService.trades$
      .pipe(takeUntil(this.destroy$))
      .subscribe(trade => {
        this.trades$.next(trade);
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
