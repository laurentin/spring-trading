import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import {
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
} from '@angular/material';

import { TradesComponent } from './trades.component';
import { TradeTableComponent } from './components/trade-table/trade-table.component';

const routes: Routes = [{ path: '', component: TradesComponent }];

@NgModule({
  declarations: [TradesComponent, TradeTableComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
  ],
})
export class TradesModule {}
