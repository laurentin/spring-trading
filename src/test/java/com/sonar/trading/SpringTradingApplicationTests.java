package com.sonar.trading;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringTradingApplication.class, properties = { "X.UI=25", "order.thread.core-pool=5",
	"order.thread.max-pool=5", "order.queue.capacity=50", "M.UP=10", "N.DW=10" })
public class SpringTradingApplicationTests {

	@Test
	public void contextLoads() {
	}

}
