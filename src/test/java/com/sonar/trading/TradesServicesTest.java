package com.sonar.trading;

import com.sonar.trading.model.Trade;
import com.sonar.trading.service.TradesServicesImpl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.sonar.trading.util.Constants.MARKER_BUY;
import static com.sonar.trading.util.Constants.MARKER_SELL;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class TradesServicesTest {

    @InjectMocks
    TradesServicesImpl tradesServices;

    @Test
    public void testCheckAndTradeOnUpticks() {
        List<Trade> sells = new ArrayList<>();

        // up > up > zero
        Trade trade = new Trade();
        trade.setCreatedAt("2018-10-15 11:50:00");
        trade.setPrice("100");
        trade.setMakerSide(MARKER_SELL);
        trade.setAmount("7");
        sells.add(trade);

        trade = new Trade();
        trade.setCreatedAt("2018-10-15 11:51:00");
        trade.setPrice("101");
        trade.setMakerSide(MARKER_SELL);
        trade.setAmount("2");
        sells.add(trade);

        trade = new Trade();
        trade.setCreatedAt("2018-10-15 11:50:00");
        trade.setPrice("101");
        trade.setMakerSide(MARKER_SELL);
        trade.setAmount("2");
        sells.add(trade);

        assertEquals(1L,tradesServices.checkAndTradeOnUpticks(sells, 2).longValue());
    }

    @Test
    public void testCheckAndTradeOnDownticks() {
        List<Trade> buys = new ArrayList<>();

        // down > zero > down
        Trade trade = new Trade();
        trade.setCreatedAt("2018-10-15 11:50:00");
        trade.setPrice("100");
        trade.setMakerSide(MARKER_BUY);
        trade.setAmount("7");
        buys.add(trade);

        trade = new Trade();
        trade.setCreatedAt("2018-10-15 11:51:00");
        trade.setPrice("99");
        trade.setMakerSide(MARKER_BUY);
        trade.setAmount("2");
        buys.add(trade);

        trade = new Trade();
        trade.setCreatedAt("2018-10-15 11:50:00");
        trade.setPrice("99");
        trade.setMakerSide(MARKER_BUY);
        trade.setAmount("2");
        buys.add(trade);

        trade = new Trade();
        trade.setCreatedAt("2018-10-15 11:50:00");
        trade.setPrice("95");
        trade.setMakerSide(MARKER_BUY);
        trade.setAmount("1");
        buys.add(trade);

        assertEquals(1L,tradesServices.checkAndTradeOnDownticks(buys, 2).longValue());
    }
}
